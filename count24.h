#ifndef COUNT24_H
#define COUNT24_H

#include <QObject>

QT_FORWARD_DECLARE_CLASS(EquationTreeNode)

const int N = 4;
class Count24 : public QObject
{
    Q_OBJECT
public:
    explicit Count24(QObject *parent = 0);
    void setInitValue(int initVal[N]);
    void setAllowReorder(bool allow);

signals:
    void calculatedOneResult(QString equation);
    void calclateDone(bool haveResult);

public slots:
    void calculate();

private:
    EquationTreeNode *initEquationTreeNode(QVector<EquationTreeNode *> numbers,
                                           QVector<EquationTreeNode *> operators,
                                           QVector<int> order);

    int m_numbers[N];
    bool m_allowReorder;
};

#endif // COUNT24_H
