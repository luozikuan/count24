#include <QString>
#include "equationtreenode.h"

// 1+2*3-4  order:1 2 3 => (1+2)*3-4
// 1+2*3-4  order:3 2 1 => 1+2*(3-4)
// 1+2*3-4  order:1 3 2 => (1+2)*(3-4)

// 用树保存一个式子，中间结点存 +-*/，叶子结点存数字
// toString 用递归，计算也用递归

EquationTreeNode::EquationTreeNode(int type, int value)
{
    setNodeValue(type, value);
    setLeft(Q_NULLPTR);
    setRight(Q_NULLPTR);
}

EquationTreeNode::~EquationTreeNode()
{
    delete left;
    delete right;
}

QString EquationTreeNode::toString()
{
    static const int CalcOrder[5] = {0, 1, 1, 2, 2};
    static const QChar OperatorsChar[5] = {'?', '+', '-', '*', '/'};

    QString result;
    if (this->nodeType == 0) {
        QString leftString = this->left->toString();
        QString rightString = this->right->toString();

        if (this->left->nodeType == 0 && CalcOrder[this->left->biOperator] < CalcOrder[this->biOperator])
            leftString = QString("(%1)").arg(leftString);
        if (this->right->nodeType == 0) {
            if (CalcOrder[this->right->biOperator] < CalcOrder[this->biOperator]
                    || (CalcOrder[this->right->biOperator] == CalcOrder[this->biOperator]
                        && (this->biOperator == Divide
                            || this->biOperator == Minus))) {
                rightString = QString("(%1)").arg(rightString);
            }
        }
        result = QString("%1%2%3").arg(leftString).arg(OperatorsChar[this->biOperator]).arg(rightString);
    } else {
        result = QString::number(this->operand);
    }
    return result;
}

double EquationTreeNode::value()
{
    if (this->nodeType == 1) {
        return this->operand;
    } else {
        double leftValue;
        double rightValue;

        QT_TRY {
            leftValue = this->left->value();
            rightValue = this->right->value();
        } QT_CATCH(const QString &e) {
            QT_RETHROW ;
        }

        switch (this->biOperator) {
        case Add:
            return leftValue + rightValue;
            break;
        case Minus:
            return leftValue - rightValue;
            break;
        case Times:
            return leftValue * rightValue;
            break;
        case Divide:
            if (qAbs(rightValue) < 1e-6) {
                QT_THROW(QString("Divide by zero!"));
            }
            return leftValue / rightValue;
            break;
        default:
            QT_THROW(QString("Invalid operator!"));
            return -1.0;
        }
    }
}

void EquationTreeNode::setNodeValue(int type, int value)
{
    this->nodeType = type;
    if (type == 0) {
        biOperator = value;
    } else {
        operand = value;
    }
}

void EquationTreeNode::setLeft(EquationTreeNode *left)
{
    this->left = left;
}

void EquationTreeNode::setRight(EquationTreeNode *right)
{
    this->right = right;
}

void EquationTreeNode::clearRelationship()
{
    setLeft(Q_NULLPTR);
    setRight(Q_NULLPTR);
}
