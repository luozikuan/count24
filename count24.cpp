#include <iterator>
#include <QDebug>
#include "count24.h"
#include "equationtreenode.h"

Count24::Count24(QObject *parent) : QObject(parent)
{

}

void Count24::setInitValue(int initVal[N])
{
    for (int i = 0; i < N; i++) {
        m_numbers[i] = initVal[i];
    }
}

void Count24::setAllowReorder(bool allow)
{
    m_allowReorder = allow;
}

void Count24::calculate()
{
    QVector<EquationTreeNode *> numberNode= {
        new EquationTreeNode(1, 0),
        new EquationTreeNode(1, 0),
        new EquationTreeNode(1, 0),
        new EquationTreeNode(1, 0),
    };

    QVector<EquationTreeNode *> operatorNode = {
        new EquationTreeNode(0, Invalid),
        new EquationTreeNode(0, Invalid),
        new EquationTreeNode(0, Invalid),
    };

    int operatorValue[3];
    int orderSub[3] = {0,1,2}; // permutation
    bool haveResult = false;

    do {
        for (operatorValue[0] = Add; operatorValue[0] <= Divide; operatorValue[0]++) {
            for (operatorValue[1] = Add; operatorValue[1] <= Divide; operatorValue[1]++) {
                for (operatorValue[2] = Add; operatorValue[2] <= Divide; operatorValue[2]++) {
                    do {
                        // TODO:
                        operatorNode[0]->setNodeValue(0, operatorValue[0]);
                        operatorNode[1]->setNodeValue(0, operatorValue[1]);
                        operatorNode[2]->setNodeValue(0, operatorValue[2]);
                        operatorNode[0]->clearRelationship();
                        operatorNode[1]->clearRelationship();
                        operatorNode[2]->clearRelationship();

                        numberNode[0]->setNodeValue(1, m_numbers[0]);
                        numberNode[1]->setNodeValue(1, m_numbers[1]);
                        numberNode[2]->setNodeValue(1, m_numbers[2]);
                        numberNode[3]->setNodeValue(1, m_numbers[3]);
                        numberNode[0]->clearRelationship();
                        numberNode[1]->clearRelationship();
                        numberNode[2]->clearRelationship();
                        numberNode[3]->clearRelationship();

                        EquationTreeNode *equation = initEquationTreeNode({numberNode[0],numberNode[1],numberNode[2],numberNode[3]},
                                                                          {operatorNode[0],operatorNode[1],operatorNode[2]},
                                                                          {orderSub[0], orderSub[1], orderSub[2]});
                        if (equation == Q_NULLPTR) {
                            qDebug() << "initEquationTreeNode fail";
                            continue;
                        }
                        double value;
                        QT_TRY {
                            value = equation->value();
                        } QT_CATCH (...) {
                            continue;
                        }

                        if (qAbs(value - 24) < 1e-6) {
                            haveResult = true;
                            QString str = equation->toString();
                            emit calculatedOneResult(str);
                        }
                    } while (std::next_permutation(std::begin(orderSub), std::end(orderSub)));
                }
            }
        }
    } while (m_allowReorder && std::next_permutation(std::begin(m_numbers), std::end(m_numbers)));

    emit calclateDone(haveResult);
}

// 1+2*3-4  order:1 2 3 => (1+2)*3-4
// 1+2*3-4  order:3 2 1 => 1+2*(3-4)
// 1+2*3-4  order:1 3 2 => (1+2)*(3-4)
EquationTreeNode *Count24::initEquationTreeNode(QVector<EquationTreeNode *> numbers,
                                                QVector<EquationTreeNode *> operators,
                                                QVector<int> order)
{
    if (operators.size() == 0) {
        Q_ASSERT(numbers.size() == 1);
        return numbers[0];
    } else if (operators.size() == 1) {
        operators[0]->setLeft(numbers[0]);
        operators[0]->setRight(numbers[1]);
        return operators[0];
    } else {
        int firstIndex = order.indexOf(0);
        EquationTreeNode *operatorNode = operators.takeAt(firstIndex);
        order.removeAt(firstIndex);
        for (int i = 0; i < order.size(); i++) order[i]--;

        QVector<EquationTreeNode*> leftOperators = operators.mid(0, firstIndex);
        QVector<EquationTreeNode*> rightOperators = operators.mid(firstIndex);

        QVector<EquationTreeNode*> leftNumbers = numbers.mid(0, firstIndex + 1);
        QVector<EquationTreeNode*> rightNumbers = numbers.mid(firstIndex + 1);

        QVector<int> leftOrder = order.mid(0, firstIndex);
        QVector<int> rightOrder = order.mid(firstIndex);

        EquationTreeNode *left = initEquationTreeNode(leftNumbers, leftOperators, leftOrder);
        EquationTreeNode *right = initEquationTreeNode(rightNumbers, rightOperators, rightOrder);
        operatorNode->setLeft(left);
        operatorNode->setRight(right);
        return operatorNode;
    }
    return Q_NULLPTR;
}
