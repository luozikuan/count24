#include <QDebug>
#include <QListWidget>
#include "widget.h"
#include "ui_widget.h"
#include "count24.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    count24(new Count24(this))
{
    ui->setupUi(this);

    connect(count24, &Count24::calculatedOneResult, this, &Widget::gotResult);
    connect(count24, &Count24::calclateDone, this, &Widget::calcDone);

    ui->spinBox->selectAll();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
        on_btnCalc_clicked();
        event->accept();
        break;
    default:
        event->ignore();
    }
    event->ignore();
}

void Widget::on_btnCalc_clicked()
{
    ui->listWidget->clear();
    ui->btnCalc->setEnabled(false);
    ui->spinBox->setFocus();
    ui->spinBox->selectAll();

    int numbers[4];
    numbers[0] = ui->spinBox->value();
    numbers[1] = ui->spinBox_2->value();
    numbers[2] = ui->spinBox_3->value();
    numbers[3] = ui->spinBox_4->value();

    count24->setInitValue(numbers);
    count24->setAllowReorder(ui->checkBox->isChecked());
    count24->calculate();
}

void Widget::gotResult(QString equationStr)
{
    if (ui->listWidget->findItems(equationStr, Qt::MatchFixedString | Qt::MatchCaseSensitive).isEmpty()) {
        ui->listWidget->addItem(equationStr);
    }
}

void Widget::calcDone(bool haveResult)
{
    qDebug() << "calc done, have result:" << haveResult;
    ui->btnCalc->setEnabled(true);
    if (!haveResult) {
        ui->listWidget->addItem(tr("No result!"));
    }
}
