#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QKeyEvent>

QT_FORWARD_DECLARE_CLASS(Count24)

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_btnCalc_clicked();
    void gotResult(QString equationStr);
    void calcDone(bool haveResult);

private:
    Ui::Widget *ui;

    Count24 *count24;
};

#endif // WIDGET_H
