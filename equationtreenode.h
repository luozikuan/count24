#ifndef EQUATIONTREENODE_H
#define EQUATIONTREENODE_H

enum BinaryOperator {
    Invalid = 0, Add, Minus, Times, Divide
};

class EquationTreeNode
{
public:
    EquationTreeNode(int type, int value);
    ~EquationTreeNode();
    QString toString();
    double value();

    void setNodeValue(int type, int value);
    void setLeft(EquationTreeNode *left);
    void setRight(EquationTreeNode *right);
    void clearRelationship();

private:
    int nodeType; // 0-operator, 1-operand
    union {
        int biOperator; // Invalid = 0, Add, Minus, Times, Divide
        int operand;
    };

    EquationTreeNode *left;
    EquationTreeNode *right;
};



#endif // EQUATIONTREENODE_H
