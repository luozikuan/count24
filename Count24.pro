#-------------------------------------------------
#
# Project created by QtCreator 2016-08-04T21:38:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Count24
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    count24.cpp \
    equationtreenode.cpp

HEADERS  += widget.h \
    count24.h \
    equationtreenode.h

FORMS    += widget.ui
